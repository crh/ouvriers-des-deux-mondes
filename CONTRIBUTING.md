# Contribution guide

## Installation

See [README.md](/README.md), section _Installation process_.

## IDE integration

Settings are available for Visual Studio Code. This includes :

- Auto-formatting on save using [black](https://github.com/psf/black) and [isort](https://github.com/PyCQA/isort)
  - Custom settings for line width of 110 characters
  - Disabled for HTML files (auto-formatting in Jinja templates doesn't work well)
- Linting/type checking enabled, using [flake8](https://flake8.pycqa.org/en/latest/), [flake8-annotations](https://github.com/sco1/flake8-annotations) and [mypy](http://mypy-lang.org/)
- Debugging
  - Use `./debug.sh <cmd>` to debug a command, e.g. `./debug.sh flask db init` or `./debug.sh flask run`

Settings for black, isort, flake8, mypy are stored in _tox.ini_ and _mypy.ini_, so any environment should also use these settings.

## Running tests

Tests can simply be ran via the command `pytest`.

They are auto-discovered from the _tests_ folder.

## Tips

### Add a table in the database

1. Add the model in _app/modeles/data.py_
2. Expose it in _app/modeles/__init__.py_
3. In _app/cmd/db.py_:
    1. Add it to the lists of tables to drop and recreate
    2. Add the logic to populate the table

### Avoid caching issues with CSS/JS

When changing CSS/JS files, you may want to avoid caching issue in production (e.g. the files have changed, but the browser/server still have/serve the old versions in cache).
The trick is to increment the `version` variable in _partials.html_ template.

If served CSS/JS are used in other templates, use the same trick.
