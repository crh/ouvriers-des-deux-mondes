# Installation guide

This guides is intended for deploying the project on a web server (e.g. production or staging).

**Notes:**

* All commands are to be run on the webserver. Use SSH to connect to the server (e.g. `ssh <username>@<host>`).
* Some commands will be ran as root/sudoer, they will start with `root@host:/path#`
* Some commands will be ran as _www-od2m_ user, they will start with `www-od2m@host:/path$`

## Pre-requisite

* Ubuntu 20.04.3 LTS with root/sudoer SSH access
  * Python 3.8 installed (check with `python3 --version`)
  * snapd installed

## Description

Deployment will use:

* Apache2 as front-end
  * Website will be available at _/_ (e.g. https://ouvriersdeuxmondes.huma-num.fr/)
  * Logging goes to _/var/log/apache2/od2m-access.log_ and _/var/log/apache2/od2m-error.log_
* WSGI to interface Apache2 with Python
  * The WSGI module will run as a dedicated user _www-od2m_
* A git checkout as source
  * Done in _/var/www/od2m_
  * As user _www-od2m_
* HTTP and HTTPS are configured
  * HTTPS is used by default,
  * HTTP redirects to HTTPS

## Installation

### Apache

Run:

```console
root@host:/# sudo apt-get install apache2
...
The following NEW packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils libapr1 libaprutil1 libaprutil1-dbd-sqlite3 libaprutil1-ldap libjansson4 liblua5.2-0 ssl-cert
0 upgraded, 11 newly installed, 0 to remove and 0 not upgraded.
...
Do you want to continue? [Y/n] Y
...
```

On the host, a web browser at http://127.0.0.1 should work (i.e. display the default home page "Apache2 Ubuntu Default Page"). If there are no browser installed, the command `wget http://127.0.0.1 && cat index.html && rm index.html` should display the source of the home page.

To check the installation from a remote browser (optional but recommended), edit _etc/apache2/sites-enabled/000-default.conf_ to uncomment/add:

```apache
ServerName ouvriersdeuxmondes.huma-num.fr
```

And restart apache:

```console
root@host:/# sudo systemctl restart apache2
```

A web browser at http://ouvriersdeuxmondes.huma-num.fr should display the default home page.

### SSL

To enable SSL, we'll use [certbot](https://certbot.eff.org/).

```console
root@host:/# sudo a2enmod ssl
...
Enabling module ssl.
...
To activate the new configuration, you need to run:
  systemctl restart apache2
root@host:/# sudo systemctl restart apache2
root@host:/# sudo snap install --classic certbot
certbot 1.21.0 from Certbot Project (certbot-eff✓) installed
root@host:/# sudo ln -s /snap/bin/certbot /usr/bin/certbot
root@host:/# sudo certbot --apache
```

The last command will ask for a user email and generate the certicicate in _/etc/letsencrypt/live/ouvriersdeuxmondes.huma-num.fr/_. This certificate will be valid only for a year or so, but should be automatically updated before expiration. To check this, run:

```console
root@host:/# sudo certbot renew --dry-run
...
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Congratulations, all simulated renewals succeeded: 
  /etc/letsencrypt/live/ouvriersdeuxmondes.huma-num.fr/fullchain.pem (success)
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```

Now, a web browser at https://ouvriersdeuxmondes.huma-num.fr (notice the httpS) should display the default home page.

### WSGI

Next we have to set-up WSGI. As recommended by [flask documentation](https://flask.palletsprojects.com/en/2.0.x/deploying/mod_wsgi/), we will use [mod_wsgi](https://github.com/GrahamDumpleton/mod_wsgi).

```console
root@host:/# sudo apt-get install apache2-dev libapache2-mod-wsgi-py3 python3-dev
...
The following NEW packages will be installed:
  apache2-dev autoconf automake autopoint autotools-dev binutils binutils-common binutils-x86-64-linux-gnu build-essential cpp cpp-9 debhelper dh-autoreconf
  dh-strip-nondeterminism dpkg-dev dwz fakeroot g++ g++-9 gcc gcc-9 gcc-9-base gettext intltool-debian libalgorithm-diff-perl libalgorithm-diff-xs-perl
  libalgorithm-merge-perl libapache2-mod-wsgi-py3 libapr1-dev libaprutil1-dev libarchive-cpio-perl libarchive-zip-perl libasan5 libatomic1 libbinutils libc-dev-bin
  libc6-dev libcc1-0 libcroco3 libcrypt-dev libctf-nobfd0 libctf0 libdebhelper-perl libdpkg-perl libexpat1-dev libfakeroot libfile-fcntllock-perl
  libfile-stripnondeterminism-perl libgcc-9-dev libgomp1 libisl22 libitm1 libldap2-dev liblsan0 libltdl-dev libmail-sendmail-perl libmpc3 libpython3-dev libpython3.8-dev
  libquadmath0 libsctp-dev libsctp1 libstdc++-9-dev libsub-override-perl libsys-hostname-long-perl libtool libtsan0 libubsan1 linux-libc-dev m4 make manpages-dev
  po-debconf python3-dev python3.8-dev uuid-dev zlib1g-dev
0 upgraded, 77 newly installed, 0 to remove and 0 not upgraded.
...
Do you want to continue? [Y/n] Y
...
root@host:/# sudo a2dismod wsgi
Module wsgi disabled.
To activate the new configuration, you need to run:
  systemctl restart apache2
```

This disables Apache's default WSGI module and will let us create it from the Python project, handling for us any virtualenv/path issues.

### User

Let's create our dedicated user. The user will not be able to log in, so no password is required. You cna leave all fields empty.

```console
root@host:/# sudo adduser www-od2m --disabled-password
Adding user `www-od2m' ...
Adding new group `www-od2m' (1001) ...
Adding new user `www-od2m' (1001) with group `www-od2m' ...
Creating home directory `/home/www-od2m' ...
Copying files from `/etc/skel' ...
Changing the user information for www-od2m
Enter the new value, or press ENTER for the default
  Full Name []: 
  Room Number []: 
  Work Phone []: 
  Home Phone []: 
  Other []: 
Is the information correct? [Y/n] Y
```

### Deployment

#### Preparation

We need to install git and virtualenv:

```console
root@host:/# sudo apt-get install git python3-virtualenv
...
The following NEW packages will be installed:
  python-pip-whl python3-appdirs python3-distlib python3-filelock python3-virtualenv
0 upgraded, 5 newly installed, 0 to remove and 0 not upgraded.
...
Do you want to continue? [Y/n] Y
...
```

#### Creating directories

Now we create the directory and transfer the ownserhip to _www-od2m_ user so it'll be able to do the checkout and write stuff.

```console
root@host:/# cd /var/www/
root@host:/var/www# sudo mkdir od2m
root@host:/var/www# sudo chown -R www-od2m:www-od2m od2m
root@host:/var/www# cd od2m/
```

#### Checking out

We can now do the checkout as _www-od2m_ user, install the dependencies and create the database:

```console
root@host:/var/www/od2m# sudo su www-od2m
www-od2m@host:/var/www/od2m$ git clone https://git.oslandia.net/Client-projects/2108_07_cnrs_dev_enquete.git .
Cloning into '.'...
remote: Enumerating objects: 228, done.
remote: Counting objects: 100% (228/228), done.
remote: Compressing objects: 100% (122/122), done.
remote: Total 6231 (delta 147), reused 166 (delta 104), pack-reused 6003
Receiving objects: 100% (6231/6231), 71.63 MiB | 10.10 MiB/s, done.
Resolving deltas: 100% (4727/4727), done.
www-od2m@host:/var/www/od2m$ virtualenv .venv
created virtual environment CPython3.8.10.final.0-64 in 84ms
...
www-od2m@host:/var/www/od2m$ source bootstrap.sh
(.venv) www-od2m@host:/var/www/od2m$ python --version
Python 3.8.10
(.venv) www-od2m@host:/var/www/od2m$ pip install -r requirements.txt
...
Successfully installed Flask-1.1.2 Jinja2-2.11.2 MarkupSafe-1.1.1 Werkzeug-1.0.1 beautifulsoup4-4.9.3 black-21.9b0 bs4-0.0.1 click-7.1.2 flake8-4.0.1 geographiclib-1.52 geopy-2.2.0 isort-5.9.3 itsdangerous-1.1.0 lxml-4.6.2 mccabe-0.6.1 mypy-0.910 mypy-extensions-0.4.3 numpy-1.21.2 pandas-1.3.2 pathspec-0.9.0 peewee-3.14.7 platformdirs-2.4.0 pycodestyle-2.8.0 pyflakes-2.4.0 python-dateutil-2.8.2 pytz-2021.1 regex-2021.10.23 six-1.16.0 soupsieve-2.1 toml-0.10.2 tomli-1.2.2 tqdm-4.62.3 typing-extensions-3.10.0.2
(.venv) www-od2m@host:/var/www/od2m$ flask db init
Fetching Monography geocodes from CSV...: 127it
Dropping existing DB...
Re-creating schema...
Populating Subtype from XML files...: 128it
Populating Authors from CSV...: 128it
Populating Monography from CSV...: 128it
Populating Inventory from XML files...: 128it
Populating Workers from CSV...: 841it
```

#### Configuring WSGI

Still as _www-od2m_ user with virtualenv loaded, we'll create the WSGI configuration:

```console
(.venv) www-od2m@host:/var/www/od2m$ pip install mod_wsgi
...
Successfully installed mod-wsgi-4.9.0
(.venv) www-od2m@host:/var/www/od2m$ mod_wsgi-express module-config > config/wsgi_od2m.load
```

Exit and come back to the root/sudoer shell:

```console
(.venv) www-od2m@host:/var/www/od2m$ exit
exit
```

#### Finalizing

We have to hook our newly-created configuration, disable the default Apache sites and enable our new one:

```console
root@host:/var/www/od2m# sudo ln -s /var/www/od2m/config/001-od2m.conf /etc/apache2/sites-available/001-od2m.conf
root@host:/var/www/od2m# sudo ln -s /var/www/od2m/config/wsgi_od2m.load /etc/apache2/mods-available/wsgi_od2m.load
root@host:/var/www/od2m# sudo a2dissite 000-default
Site 000-default disabled.
To activate the new configuration, you need to run:
  systemctl reload apache2
root@host:/var/www/od2m# sudo a2dissite 000-default-le-ssl.conf
Site 000-default-le-ssl disabled.
To activate the new configuration, you need to run:
  systemctl reload apache2
root@host:/var/www/od2m# sudo a2ensite 001-od2m
Enabling site 001-od2m.
To activate the new configuration, you need to run:
  systemctl reload apache2
root@host:/var/www/od2m# sudo a2enmod wsgi_od2m
Enabling module wsgi_od2m.
To activate the new configuration, you need to run:
  systemctl restart apache2
root@host:/var/www/od2m# sudo systemctl restart apache2
```

https://ouvriersdeuxmondes.huma-num.fr should now display the app. If there are any errors, they will be logged to _/var/log/apache2/od2m-error.log_.

## Updates

### Git

To update the web site after an update of the git repo:

1. Log as _www-od2m_ user:
    1. Pull changes
    2. Install new python dependencies (if any)
    3. Recreate DB (if required)
2. Restart apache

```console
root@host:/# cd /var/www/od2m/
root@host:/var/www/od2m# sudo su www-od2m
www-od2m@host:/var/www/od2m$ git fetch
...
www-od2m@host:/var/www/od2m$ git reset --hard origin/main
HEAD is now at <commit_sha1> <commit_msg>
www-od2m@host:/var/www/od2m$ source bootstrap.sh
(.venv) www-od2m@host:/var/www/od2m$ pip install -r requirements.txt
...
(.venv) www-od2m@host:/var/www/od2m$ flask db init
...
(.venv) www-od2m@host:/var/www/od2m$ exit
exit
root@host:/var/www/od2m# sudo service apache2 restart
```

### System

If there are changes on the system (e.g. version of apache2, python), you may need to set-up WSGI again:

1. As _www-od2m_ user:
    1. Potentially, remove previous virtual environment _.venv_ and re-create it (required if python changed)
    2. Upgrade mod_wsgi
    3. Recreate configuration
2. Restart Apache

```console
root@host:/# cd /var/www/od2m/
root@host:/var/www/od2m# sudo su www-od2m
www-od2m@host:/var/www/od2m$ rm -rf .venv
www-od2m@host:/var/www/od2m$ virtualenv .venv
www-od2m@host:/var/www/od2m$ source bootstrap.sh
(.venv) www-od2m@host:/var/www/od2m$ pip install -r requirements.txt
...
(.venv) www-od2m@host:/var/www/od2m$ pip install mod_wsgi --upgrade
...
(.venv) www-od2m@host:/var/www/od2m$ mod_wsgi-express module-config > config/wsgi_od2m.load
(.venv) www-od2m@host:/var/www/od2m$ exit
exit
root@host:/var/www/od2m# sudo service apache2 restart
```

## Common errors

### Cannot run `flask db init`: `peewee.OperationalError: disk I/O error`

This can happen if the DB is locked (open from another thread for instance).

Simply remove the DB and relaunch the command:

```console
(.venv) www-od2m@host:/var/www/od2m$ rm od2m_bdd.sqlite od2m_bdd.sqlite-shm od2m_bdd.sqlite-wal
(.venv) www-od2m@host:/var/www/od2m$ flask db init
...
```
