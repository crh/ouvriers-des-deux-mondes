# Les Ouvriers des deux mondes

_Les Ouvriers des deux mondes_ are a collection of sociological surveys published by Frédéric Le Play (†1882) and the International Society of socio-economic practical studies. Those monographs were assembled in three sets composed of thirteen volumes from 1857 to 1913, with an addition of two booklets published in 1930. The ANR “Time Us” program undertook the transcription and structuration of the monographs following the TEI standard in order to make it possible to consult and study them digitally.

Initial acquisition and structuration of the texts were automatically produced based on the digitization of _[Internet Archive](http://timeusage.paris.inria.fr/mediawiki/index.php/Aper%C3%A7u_des_%C3%A9tats#Les_Ouvriers_des_Mondes)_, using [the LSE-OD2M app](https://gitlab.inria.fr/almanach/time-us/LSE-OD2M) developed by Alix Chagué, Research and Development engineer of the [ALMAnaCH team at Inria](https://team.inria.fr/almanach/fr/). Scientific encoding was then carry out by Jean-Damien Généro, Studies Engineer at CNRS, first welcomed at the [Centre Maurice Halbwachs](https://www.cmh.ens.fr/) (CMH) and then assigned at the [Centre de recherches historiques](http://crh.ehess.fr/) (CRH) under the supervision of Anne Lhuissier, Research Director at INRAE (CMH) and Stéphane Baciocchi, Research Engineer from EHESS (CRH).

The goal of the app is to present the advance of the work on the monographs. It is currently developed by Jean-Damien Généro.

---

## Features

- List of the monographs ;
- Visualisation of the monographs’ text with a link to the IIIF images of the pages ;
- Search in monographs' text (§6, 8, 10).

---

## Installation process

This process describes how to install and run the project locally, e.g. for development purposes.

To deploy the project on a web server (e.g. production, staging, etc.), please refer to [INSTALL.md](/INSTALL.md).

_Nota_ : commands to execute through the terminal (Linux or macOS)

- Clone the repo : `git clone https://git.oslandia.net/Client-projects/2108_07_cnrs_dev_enquete.git`
- Install the virtual environment:
  - Check that you’re using Python 3.x version by running : `python --version` (>3.8 required);
  - Go to the following directory  : `cd app-ouvriers-deux-mondes`;
  - Install the environment : `python3 -m venv .venv`.
  - Install the packages and libraries :
    - Activate the environment  : `source bootstrap.sh`;
    - Installation : `pip install -r requirements.txt`
    - Create database: `flask db init`;
    - Exit the environment : `deactivate`;

## Data processing

When data is changed (e.g. XML files or CSV files), the following commands must be ran **in this order**:

1. `flask geocode run` to recreate _static/csv/geocoder.csv_ file
    - only required if fields "Settlement", "Region", "Country" have changed in _static/csv/id\_monographies.csv_ or if new XML files are added there
    - don't forget to commit the changes to this file afterwards
2. `flask db init` to recrete the DB from the CSV files (_static/csv/id\_monographies.csv_ and _static/csv/O2M\_prosopo\_enquetees.csv_) and XML files (from _static/xml_)

## Launch
  
- Activate the environment : `source bootstrap.sh`;
- Launch : `flask run`;
- Go to http://127.0.0.1:5000/;
- Stop : `ctrl + c`;
- Exit the environment : `deactivate`.

---

[fr] _Les_ Ouvriers des deux mondes _sont des recueils d'enquêtes sociologiques publiées sous l'égide de Frédéric Le Play (†1882) et de la Société internationale des études pratiques d'économie sociale. Ces monographies ont été rassemblées en trois séries de treize volumes de 1857 à 1913, deux fascicules supplémentaires paraissant en 1930. Le programme ANR « [Time Us](http://larhra.ish-lyon.cnrs.fr/anr-time-us) » a entrepris de transcrire et de structurer les monographies au standard TEI afin de permettre leur consultation électronique et leur étude._

_L'acquisition et la structuration initiale des textes ont été réalisées automatiquement à partir des [numérisations d'Internet Archive](http://timeusage.paris.inria.fr/mediawiki/index.php/Aper%C3%A7u_des_%C3%A9tats#Les_Ouvriers_des_Mondes), à l'aide de l'application [LSE-OD2M](https://gitlab.inria.fr/almanach/time-us/LSE-OD2M) développée par Alix Chagué, ingénieure de recherche et de développement de l'[équipe ALMAnaCH d'Inria](https://team.inria.fr/almanach/fr/). L'encodage scientifique a ensuite été réalisé par Jean-Damien Généro, ingénieur d'études du CNRS accueilli au [Centre Maurice Halbwachs](https://www.cmh.ens.fr/) (CMH) puis affecté au [Centre de recherches historiques](http://crh.ehess.fr/) (CRH) sous la supervision d'Anne Lhuissier, directrice de recherche de l'INRAE (CMH) et de Stéphane Baciocchi, ingénieur de recherche de l'EHESS (CRH)._

_Cette application a pour but de présenter un état du travail sur les monographies. Elles est développée par Jean-Damien Généro._
