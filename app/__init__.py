import typing as t

import werkzeug
from flask import render_template

from .app import app
from .cmd import db_cli, geocode_cli
from .routes import main

app.register_blueprint(main)
app.cli.add_command(db_cli)
app.cli.add_command(geocode_cli)


@app.errorhandler(404)
def page_not_found(e: werkzeug.exceptions.HTTPException) -> t.Tuple[t.Text, int]:
    return render_template("404.html", title="Page non trouvée"), 404


@app.errorhandler(500)
def internal_server_error(e: werkzeug.exceptions.HTTPException) -> t.Tuple[t.Text, int]:
    return render_template("500.html", title="Erreur interne du serveur"), 500
