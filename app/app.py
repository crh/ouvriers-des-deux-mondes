import os

from flask import Flask
from playhouse.sqlite_ext import SqliteExtDatabase

from .debugger import initialize_flask_server_debugger_if_needed

APPPATH = os.path.dirname(os.path.abspath(__file__))
templates = os.path.join(APPPATH, "templates")
statics = os.path.join(APPPATH, "static")


app = Flask(
    "Application",
    template_folder=templates,
    static_folder=statics,
)

# DB configuration
app.config["DATABASE"] = os.path.join(APPPATH, "..", "od2m_bdd.sqlite")
db = SqliteExtDatabase(app.config["DATABASE"], pragmas=[("journal_mode", "wal")])

# Enables debugging in VS Code if DEBUG env var is set
_debugging = initialize_flask_server_debugger_if_needed()
