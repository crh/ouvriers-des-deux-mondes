import csv
import os
import re

def add_att(csv_mono, path) -> None:
    """
    add attribute @corresp to <ref> with a target="#ID-..."
    (= ref that refers to other xml file,
    with a @type="renvoi_int_od2m").
    The @correp have the short title as value,
    from the csv file.
    :param csv_mono: 
    :param path: path to /app/static/xml
    :type path: str
    """
    with open(csv_mono, 'r') as csv_file:
        spamreader = csv.reader(csv_file)
        filedict = {row[5]: row[1] for row in spamreader}
    for file in os.listdir(path):
        if '.xml' in file:
            xmlfile = os.path.join(path, file)
            with open(xmlfile, 'r', encoding='utf-8') as opening:
                xml = opening.read()
                for item in filedict:
                    # two cases : #ID- and #ID_
                    xml = xml.replace(
                        '<ref target="#ID-' + filedict[item] + '"',
                        '<ref corresp="' + item +'" target="#ID-' + filedict[item]+ '"'
                        )
                    xml = xml.replace(
                        '<ref target="#ID_' + filedict[item] + '"',
                        '<ref corresp="' + item +'" target="#ID_' + filedict[item]+ '"'
                        )
            with open(xmlfile, 'w') as writting:
                 writting.write(str(xml))


add_att("../static/csv/id_monographies.csv", "../static/xml")
