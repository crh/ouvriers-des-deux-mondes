import csv
import os
import typing as t

from bs4 import BeautifulSoup
from flask.cli import AppGroup
from lxml import etree
from tqdm import tqdm

from app.app import APPPATH, db
from app.clear_xml import clear_file
from app.modeles import Author, FTSMonography, Inventory, Monography, Subtype, Type, Worker
from app.utils import get_filenames

db_cli = AppGroup("db")


def _nonempty(s: str) -> t.Optional[str]:
    """Returns content only if non-empty; otherwise returns None"""
    if s:
        return s
    else:
        return None


def _capitalize_first(s: t.Optional[str]) -> t.Optional[str]:
    return (s[0].upper() + s[1:]) if s else None


def _create_types() -> t.List[Type]:
    types = [
        Type(label="proprietes", nice_label="inventaires des propriétés"),
        Type(label="travaux", nice_label="descriptions des travaux entrepris par les membres de la famille"),
        Type(label="industries", nice_label="industries entreprises par la famille"),
        Type(label="par_10", nice_label="inventaires de l'habitation, du mobilier et des vêtements"),
    ]
    for type in types:
        type.save()
    return types


def _create_subtypes(files_list: t.List[str], types: t.List[Type]) -> t.Dict[t.Tuple[str, str], Subtype]:
    types_dict = {x.label: x for x in types}
    subtypes_dict: t.Dict[t.Tuple[str, str], Subtype] = {}
    for item in tqdm(files_list, desc="Populating Subtype from XML files..."):
        with open(item) as xml:
            xmlfile = xml.read()
            soup = BeautifulSoup(xmlfile, "xml")
            subtypes = soup.find_all("div", subtype=True)
            for tag in subtypes:
                if tag["type"] not in types_dict:
                    print(f"Type {tag['type']} cannot be found, skipping subtype {tag['subtype']}")
                    continue

                if (tag["type"], tag["subtype"]) not in subtypes_dict:
                    subtype = Subtype(label=tag["subtype"], type=types_dict[tag["type"]])
                    subtypes_dict[tag["type"], tag["subtype"]] = subtype
                    subtype.save()
    return subtypes_dict


def _create_authors() -> t.Dict[t.Tuple[str, str], Author]:
    authors_dict: t.Dict[t.Tuple[str, str], Author] = {}
    with open(os.path.join(APPPATH, "static", "csv", "authors.csv"), "r", encoding="utf-8") as f:
        csvreader = csv.DictReader(f, delimiter=";")
        for line in tqdm(csvreader, "Populating Authors from CSV..."):
            author = Author(
                                firstname=line["Forname"],
                                lastname=line["Surname"],
                                date_birth=line["date_birth"],
                                date_death=line["date_death"],
                                quality=_capitalize_first(_nonempty(line["Quality"])),
                                id_viaf=_nonempty(line["viaf"]),
                                id_isni=_nonempty(line["isni"]),
                                id_idref=_nonempty(line["idref"]),
                                id_wikipedia=_nonempty(line["wikipedia"]),
                                id_wikidata=_nonempty(line["wikidata"]),
                                id_bnf=_nonempty(line["bnf"]),
                                id_lc=_nonempty(line["lcnaco"]),
                                id_dnb=_nonempty(line["dnb"]),
                                id_nta=_nonempty(line["nta"]),
                            )
            authors_dict[author.firstname, author.lastname] = author
            author.save()
    """with open(os.path.join(APPPATH, "static", "csv", "id_monographies.csv"), "r", encoding="utf-8") as f:
                    csvreader = csv.DictReader(f)
                    for line in tqdm(csvreader, "Populating Authors from CSV..."):
                        if line["Forname1"] and (line["Forname1"], line["Surname1"]) not in authors_dict:
                            author1 = Author(
                                firstname=line["Forname1"],
                                lastname=line["Surname1"],
                                quality=_capitalize_first(_nonempty(line["Quality1"])),
                                id_viaf=_nonempty(line["id_viaf1"]),
                                id_isni=_nonempty(line["id_isni1"]),
                                id_idref=_nonempty(line["id_idref1"]),
                                id_wikipedia=_nonempty(line["id_wikipedia1"]),
                                id_wikidata=_nonempty(line["id_wikidata1"]),
                            )
                            authors_dict[author1.firstname, author1.lastname] = author1
                            author1.save()
            
                        if line["Forname2"] and (line["Forname2"], line["Surname2"]) not in authors_dict:
                            author2 = Author(
                                firstname=line["Forname2"],
                                lastname=line["Surname2"],
                                quality=_capitalize_first(_nonempty(line["Quality2"])),
                                id_viaf=_nonempty(line["id_viaf2"]),
                                id_isni=_nonempty(line["id_isni2"]),
                                id_idref=_nonempty(line["id_idref2"]),
                                id_wikipedia=_nonempty(line["id_wikipedia2"]),
                                id_wikidata=_nonempty(line["id_wikidata2"]),
                            )
                            authors_dict[author2.firstname, author2.lastname] = author2
                            author2.save()"""

    return authors_dict


def _create_monographies(
    geocodes: t.Dict[str, t.Dict[str, t.Any]], authors_dict: t.Dict[t.Tuple[str, str], Author]
) -> t.List[Monography]:
    monographies: t.List[Monography] = []
    with open(os.path.join(APPPATH, "static", "csv", "id_monographies.csv"), "r", encoding="utf-8") as f:
        csvreader = csv.DictReader(f)
        for line in tqdm(csvreader, "Populating Monography from CSV..."):
            if line["Files XML"] == "none":
                continue

            if not os.path.exists(os.path.join(APPPATH, "static", "xml", line["Files XML"])):
                print(
                    f"File {line['Files XML']} does not exist for monography {line['Num']}, skipping this entry"
                )
                continue

            if not os.path.exists(
                os.path.join(APPPATH, "static", "pdf", line["Files XML"].replace(".xml", ".pdf"))
            ):
                print(f"File {line['Files XML']} does not have a PDF file, skipping this entry")
                continue

            if line["Files XML"] not in geocodes:
                print(f"File {line['Files XML']} does not have geocoding, skipping this entry")
                continue

            geocode = geocodes[line["Files XML"]]

            monography = Monography(
                public_id=line["Num"],
                private_id=line["ID"],
                slug=line["Short_title2"],
                filename=line["Files XML"],
                title=line["Titles"],
                short_title=line["Short_title1"],
                author1=authors_dict[line["Forname1"], line["Surname1"]],
                author2=(
                    authors_dict[line["Forname2"], line["Surname2"]]
                    if (line["Forname2"], line["Surname2"]) in authors_dict
                    else None
                ),
                survey_year=line["Survey date"],
                booklet_year=line["Booklet date"],
                book_year=line["Book date"],
                location=geocode["location"],
                full_location=geocode["full_location"],
                lat=geocode["lat"],
                lng=geocode["lng"],
                doi=line["DOI"],
            )
            _load_content_monography(monography)
            _generate_toc_monography(monography)
            monographies.append(monography)
            monography.save()
    return monographies


def _load_content_monography(monography: Monography) -> None:
    filename = os.path.join(APPPATH, "static", "xml", monography.filename)
    clear_file(filename)
    source_doc = etree.parse(filename)
    # xpath query for selecting all element nodes in namespace
    query = "descendant-or-self::*[namespace-uri()!='']"
    for element in source_doc.xpath(query):
        # replace element name with its local name
        element.tag = etree.QName(element).localname
    etree.cleanup_namespaces(source_doc)
    xslt_doc = etree.parse(os.path.join(APPPATH, "static", "xsl", "mono_od2m.xsl"))
    xslt_transformer = etree.XSLT(xslt_doc)
    content = xslt_transformer(source_doc)
    monography.content = str(content)


def _generate_toc_monography(monography: Monography) -> None:
    soup = BeautifulSoup(monography.content, "html.parser")
    headers = soup.find_all(["h1", "h2", "h3", "h4", "h5", "h6"])
    toc = "<ul>"
    openlevel = None
    for h in headers:
        if not h.contents:
            continue
        firstchild = h.contents[0]
        if firstchild == "Sommaire" or firstchild == "Notes":
            continue

        level = int(h.name.replace("h", ""))
        if openlevel and level > openlevel:
            toc += "<ul>"
        elif openlevel and level < openlevel:
            toc += "</ul></li>" * (openlevel - level)
        elif openlevel:
            toc += "</li>"

        toc += f'<li><a href="#{firstchild.attrs["id"]}">{firstchild.text}</a>'
        openlevel = level
    if openlevel:
        toc += "</li></ul>" * openlevel
    monography.toc = toc


def _create_inventories(
    files_list: t.List[str],
    types: t.List[Type],
    subtypes_dict: t.Dict[t.Tuple[str, str], Subtype],
    monographies: t.List[Monography],
) -> None:
    types_dict = {x.label: x for x in types}
    monographies_dict = {x.filename: x for x in monographies}
    for item in tqdm(files_list, desc="Populating Inventory from XML files..."):
        with open(item) as xml:
            xmlfile = xml.read()
            soup = BeautifulSoup(xmlfile, "xml")
            subtype = soup.find_all("div", subtype=True)
        for tag in subtype:
            tag["source"] = item.replace(os.path.join(APPPATH, "static", "xml") + "/", "")
            Inventory.create(
                content=str(tag).replace("xml:id", "source-id"),
                monography=monographies_dict[tag["source"]],
                type=types_dict[tag["type"]],
                subtype=subtypes_dict[tag["type"], tag["subtype"]],
            )


def _get_geocodes() -> t.Dict[str, t.Dict[str, t.Any]]:
    geocodes = {}
    with open(os.path.join(APPPATH, "static", "csv", "geocoder.csv"), "r", encoding="utf-8") as f:
        csvreader = csv.DictReader(f)
        for line in tqdm(csvreader, "Fetching Monography geocodes from CSV..."):
            if line["Files XML"] == "none":
                continue

            geocodes[line["Files XML"]] = line
    return geocodes


def _create_workers(monographies: t.List[Monography]) -> None:
    monographies_dict = {x.private_id: x for x in monographies}
    with open(
        os.path.join(APPPATH, "static", "csv", "O2M_prosopo_enquetees.csv"), "r", encoding="utf-8"
    ) as f:
        csvreader = csv.DictReader(f)
        for line in tqdm(csvreader, "Populating Workers from CSV..."):
            if line["Id_Monographie"] not in monographies_dict:
                print(f"Monography {line['Id_Monographie']} does not exist, skipping this entry")
                continue

            worker = Worker(
                private_id=line["Id_Enqueté"],
                monography=monographies_dict[line["Id_Monographie"]],
                reference_year=line["Date de référence pour les calculs"],
                firstname=line["Prénom"],
                lastname=line["Nom"],
                quality=_nonempty(line["Place dans la famille"]),
                married=_nonempty(line["Mariage"]),
                married_year=_nonempty(line["Date de mariage calculée"]),
                birth_place=_nonempty(line["Lieu de naissance"]),
                birth_year=_nonempty(line["Date de naissance calculée"]),
                age=_nonempty(line["Age"]),
            )
            worker.save()


@db_cli.command()
def init() -> None:
    """Initialization of the database"""
    files = get_filenames(os.path.join(APPPATH, "static", "xml"), ".xml")
    geocodes = _get_geocodes()

    print("Dropping existing DB...")
    db.drop_tables([Inventory, Monography, FTSMonography, Type, Subtype, Author, Worker])
    print("Re-creating schema...")
    db.create_tables([Inventory, Monography, FTSMonography, Type, Subtype, Author, Worker])

    types = _create_types()
    subtypes_dict = _create_subtypes(files, types)
    authors_dict = _create_authors()
    monographies = _create_monographies(geocodes, authors_dict)
    _create_inventories(files, types, subtypes_dict, monographies)
    _create_workers(monographies)
