import csv
import os

from flask.cli import AppGroup
from geopy.geocoders import Nominatim
from tqdm import tqdm

from app.app import APPPATH

geocode_cli = AppGroup("geocode")


@geocode_cli.command()
def run() -> None:
    """Creates geocoder.csv file from id_monographies.csv"""
    geolocator = Nominatim(user_agent="o2m", timeout=5)

    with open(os.path.join(APPPATH, "static", "csv", "id_monographies.csv"), "r", encoding="utf-8") as f:
        csvreader = csv.DictReader(f)
        with open(os.path.join(APPPATH, "static", "csv", "geocoder.csv"), "w", encoding="utf-8") as w:
            csvwriter = csv.writer(w)
            csvwriter.writerow(["Num", "Files XML", "location", "full_location", "lat", "lng"])

            for line in tqdm(csvreader, "Geocoding from CSV..."):
                if line["Files XML"] == "none":
                    continue

                if not os.path.exists(os.path.join(APPPATH, "static", "xml", line["Files XML"])):
                    print(
                        f"File {line['Files XML']} does not exist for monography {line['Num']}, skipping this entry"
                    )
                    continue

                query = f"{line['Settlement']}, {line['Region']}, {line['Country']}"
                location = geolocator.geocode(query, language="fr-FR")
                nice_location = (
                    f"{line['Settlement']} ({line['Region']}, {line['Country']})"
                    if (line["Settlement"] != line["Region"] and line["Region"] != line["Country"])
                    else f"{line['Settlement']} ({line['Country']})"
                )
                if not location:
                    print(f"Could not geocode {line['Num']}, marking as unknown")
                    location = {"address": "Inconnu", "latitude": None, "longitude": None}

                print([
                        line["Num"],
                        line["Files XML"],
                        nice_location,
                        location.address,
                        location.latitude,
                        location.longitude,
                    ])

                csvwriter.writerow(
                    [
                        line["Num"],
                        line["Files XML"],
                        nice_location,
                        location.address,
                        location.latitude,
                        location.longitude,
                    ]
                )
