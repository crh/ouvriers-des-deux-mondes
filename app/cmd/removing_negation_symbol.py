#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Affiliation : French National Center for Scientific Research (CNRS)
Assigned at the Centre de recherches historiques (CRH, UMR 8558)
Date : 2021-11-24
Update : 
"""

import os


def remove_neg_symb(path)-> None:
    """Remove negation symbol (¬) in all xml files in /app/static/xml.
    :param path: path to /app/static/xml
    :type path: str
    """
    for file in os.listdir(path):
        if '.xml' in file:
            xmlfile = os.path.join(path, file)
            with open(xmlfile, 'r', encoding='utf-8') as opening:
                reading = opening.read()
                result = reading.replace('¬', '')
            with open(xmlfile, 'w', encoding='utf-8') as writting:
                writting.write(result)
                print("{} --> done".format(xmlfile))
