from os import getenv

# From https://blog.theodo.com/2020/05/debug-flask-vscode/


def initialize_flask_server_debugger_if_needed() -> bool:
    if getenv("DEBUGGER") == "True":
        import multiprocessing

        process = multiprocessing.current_process()
        if process.pid and process.pid > 1:
            import debugpy

            debugpy.listen(("0.0.0.0", 10001))
            print("VS Code debugger can now be attached", flush=True)
            debugpy.wait_for_client()
            print("VSCode debugger attached", flush=True)
            return True

    return False
