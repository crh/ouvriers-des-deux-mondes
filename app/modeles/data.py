import re
import typing as t

import peewee
from flask import url_for
from playhouse.sqlite_ext import FTS5Model, RowIDField, SearchField

from app.app import db


class BaseModel(peewee.Model):
    class Meta:
        database = db


class Type(BaseModel):
    id = peewee.AutoField()
    label = peewee.CharField()
    nice_label = peewee.CharField()


class Subtype(BaseModel):
    id = peewee.AutoField()
    label = peewee.CharField()
    type = peewee.ForeignKeyField(Type, backref="subtypes")


class Author(BaseModel):
    id = peewee.AutoField()
    firstname = peewee.CharField()
    lastname = peewee.CharField()
    date_birth = peewee.CharField(null=True)
    date_death = peewee.CharField(null=True)
    quality = peewee.CharField(null=True)
    id_viaf = peewee.CharField(null=True)
    id_isni = peewee.CharField(null=True)
    id_idref = peewee.CharField(null=True)
    id_wikipedia = peewee.CharField(null=True)
    id_wikidata = peewee.CharField(null=True)
    id_bnf = peewee.CharField(null=True)
    id_lc = peewee.CharField(null=True)
    id_dnb = peewee.CharField(null=True)
    id_nta = peewee.CharField(null=True)
    """firstname = peewee.CharField()
                lastname = peewee.CharField()
                quality = peewee.CharField(null=True)
                id_viaf = peewee.CharField(null=True)
                id_isni = peewee.CharField(null=True)
                id_idref = peewee.CharField(null=True)
                id_wikipedia = peewee.CharField(null=True)
                id_wikidata = peewee.CharField(null=True)"""

    @property
    def name(self: "Author") -> str:
        return f"{self.firstname} {self.lastname}"

    @property
    def link(self: "Author") -> str:
        return f'<a href="{ url_for("main.authors", _anchor="author_" + str(self.id)) }">{self.name}</a>'

    @property
    def monographies(self: "Author") -> t.List["Monography"]:
        return sorted(self.monographies1 + self.monographies2, key=lambda x: x.id)


class Monography(BaseModel):
    id = peewee.AutoField()
    public_id = peewee.CharField(unique=True, null=False)  # 48, 57 bis, etc.
    private_id = peewee.CharField(unique=True, null=False)  # 005a, 085b, etc.
    slug = peewee.CharField(unique=True, null=False)
    filename = peewee.CharField()  # As in static/xml folder
    title = peewee.TextField()
    short_title = peewee.CharField()
    author1 = peewee.ForeignKeyField(Author, backref="monographies1")
    author2 = peewee.ForeignKeyField(Author, backref="monographies2", null=True)
    survey_year = peewee.CharField()
    booklet_year = peewee.CharField()
    book_year = peewee.CharField()
    content = peewee.TextField()  # HTML
    toc = peewee.TextField()  # HTML
    location = peewee.TextField()
    full_location = peewee.TextField()  # As given by geocoding, potentially not in French or latin alphabet
    lat = peewee.DecimalField(null=True)
    lng = peewee.DecimalField(null=True)
    doi = peewee.TextField()

    def save(self: "Monography", *args: t.Any, **kwargs: t.Any) -> t.Any:
        ret = super(Monography, self).save(*args, **kwargs)
        FTSMonography.store_monography(self)
        return ret

    def tooltip(self: "Monography") -> str:
        return f"""<div class="container-fluid">
                <h4 class="text-center"><a href="{url_for('main.txt_mono', slug=self.slug)}">Monographie n°{self.public_id}<br><small>{self.short_title}</small></a></h4>
                <dl class="row g-0">
                    <dt class="col-sm-3">Enquêteur(s)</dt>
                    <dd class="col-sm-9 m-0">{self.author1.link} {', ' + self.author2.link if self.author2 else ''}</dd>

                    <dt class="col-sm-3">Année de l'enquête</dt>
                    <dd class="col-sm-9 m-0">{self.survey_year}</dd>

                    <dt class="col-sm-3">Lieu</dt>
                    <dd class="col-sm-9 m-0">{self.location}</dd>
                </dl>
            </div>"""


class FTSMonography(FTS5Model):
    HTML_RE = re.compile("<.+?>")
    rowid = RowIDField()
    title = SearchField()
    content = SearchField()

    class Meta:
        database = db

    @classmethod
    def store_monography(cls: t.Type["FTSMonography"], monography: Monography) -> None:
        content = FTSMonography.get_search_content(monography)
        try:
            FTSMonography.get(FTSMonography.rowid == monography)
        except FTSMonography.DoesNotExist:
            FTSMonography.create(rowid=monography.id, title=monography.title, content=content)
        else:
            (
                FTSMonography.update(title=monography.title, content=content)
                .where(FTSMonography.rowid == monography.id)
                .execute()
            )

    @staticmethod
    def get_search_content(monography: Monography) -> str:
        return FTSMonography.HTML_RE.sub("", monography.content)


class Inventory(BaseModel):
    id = peewee.AutoField()
    content = peewee.TextField()
    monography = peewee.ForeignKeyField(Monography, backref="inventories")
    type = peewee.ForeignKeyField(Type, backref="inventories")
    subtype = peewee.ForeignKeyField(Subtype, backref="inventories")


class Worker(BaseModel):
    id = peewee.AutoField()
    private_id = peewee.CharField(unique=True, null=False)  # 001aE1, 066aE4, etc.
    monography = peewee.ForeignKeyField(Monography, backref="workers")
    reference_year = peewee.CharField()
    firstname = peewee.CharField()
    lastname = peewee.CharField()
    quality = peewee.CharField(null=True)
    married = peewee.CharField(null=True)
    married_year = peewee.CharField(null=True)
    birth_place = peewee.CharField(null=True)
    birth_year = peewee.CharField(null=True)
    age = peewee.CharField(null=True)
