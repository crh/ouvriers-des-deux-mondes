import os
import re
import typing as t

import peewee
from flask import Blueprint, abort, render_template, request, send_from_directory
from playhouse.flask_utils import PaginatedQuery

from .app import APPPATH
from .modeles import Author, FTSMonography, Inventory, Monography, Subtype, Type

RESULT_PAR_PAGES = 5

main = Blueprint("main", __name__, url_prefix="/")


@main.route("/")
def home() -> t.Text:
    """Displays the home page"""
    corpus = Monography.select().order_by(Monography.id)
    return render_template("home.html", corpus=corpus, fluid=True)


@main.route("/monographies")
def monographies() -> t.Text:
    """Displays the list of monographies"""
    corpus = Monography.select().order_by(Monography.id)
    return render_template("corpus.html", corpus=corpus, title="Les monographies")


@main.route("/monographie/<string:slug>")
def txt_mono(slug: str) -> t.Text:
    """Displays the monography"""
    try:
        monography = Monography.get(Monography.slug == slug)
    except Monography.DoesNotExist:
        abort(404)

    prev_monography = Monography.get_or_none(Monography.id == monography.id - 1)
    next_monography = Monography.get_or_none(Monography.id == monography.id + 1)

    content = monography.content.replace(
        '<div id="toc"><h3>Sommaire</h3></div>', '<div id="toc"><h3>Sommaire</h3>' + monography.toc + "</div>"
    )
    return render_template(
        "mono.html",
        monography=monography,
        content=content,
        prev=prev_monography,
        next=next_monography,
        title=monography.short_title,
    )


@main.route("/monographie/<string:slug>/download/xml")
def download_xml(slug: str) -> t.Text:
    """Downloads a monography as XML"""
    try:
        monography = Monography.get(Monography.slug == slug)
    except Monography.DoesNotExist:
        abort(404)

    return send_from_directory(
        os.path.join(APPPATH, "static", "xml"),
        monography.filename,
        as_attachment=True,
        download_name=f"{monography.public_id}-{monography.short_title}.xml",
    )


@main.route("/monographie/<string:slug>/download/pdf")
def download_pdf(slug: str) -> t.Text:
    """Downloads a monography as PDF"""
    try:
        monography = Monography.get(Monography.slug == slug)
    except Monography.DoesNotExist:
        abort(404)

    return send_from_directory(
        os.path.join(APPPATH, "static", "pdf"),
        monography.filename.replace(".xml", ".pdf"),
        as_attachment=True,
        download_name=f"{monography.public_id}-{monography.short_title}.pdf",
    )


@main.route("/simple-search")
def search() -> t.Text:
    """Displays the search page."""
    subtype_tble = Subtype.select(Subtype, Type).join(Type).order_by(Subtype.label)
    subtypes: t.Dict[str, t.List[Subtype]] = {}
    for subtype in subtype_tble:
        if subtype.type.label not in subtypes:
            subtypes[subtype.type.label] = []
        subtypes[subtype.type.label].append(subtype)
    return render_template("search-simple.html", subtypes=subtypes, title="Recherche avancée")


@main.route("/simple-search/<string:where>")
def results(where: str) -> t.Text:
    """Displays the search results page."""
    term = request.args.get("term")
    if not term:
        abort(404)

    try:
        type = Type.get(Type.label == where)
        subtype = Subtype.get(Subtype.id == term)
    except (Type.DoesNotExist, Subtype.DoesNotExist):
        abort(404)

    if subtype.type != type:
        # Sous-type n'appartient pas au type
        abort(404)

    query = (
        Inventory.select(Inventory, Monography)
        .join(Monography)
        .where(Inventory.type == type, Inventory.subtype == term)
        .order_by(Inventory.monography)
    )

    paginated_query = PaginatedQuery(
        query,
        paginate_by=RESULT_PAR_PAGES,
    )
    count = query.count()

    return render_template(
        "results-simple.html",
        where=where,
        term=term,
        type=type,
        subtype=subtype,
        results=paginated_query,
        count=count,
        title=f'{count} résultats de la recherche pour "{subtype.label}" dans les {type.nice_label}',
    )


@main.route("/search")
def searchfts() -> t.Text:
    """Displays the full-text search results"""
    term = request.args.get("term")
    if not term:
        return render_template("search-full.html", title="Recherche")

    if "'" in term and '"' not in term:
        # Unquoted single quote
        # Not supported by FTS5, would generate a OperationError: fts5: syntax error near "'"

        # So we quote the nearest word that has a single quote
        # This will handle most of the simple cases
        term = re.sub(r"(\w+'\w+)", r'"\1"', term)

    if '"' in term and term.count('"') % 2 == 1:
        # There is an unbalanced number of quotes, so we try to fix that by adding one at the beginning or end
        # This should fix most of the simple cases
        if term.endswith('"'):
            term = '"' + term
        else:
            term = term + '"'

    query = (
        Monography.select(
            Monography,
            FTSMonography.rank().alias("score"),
            FTSMonography.title.highlight("<mark>", "</mark>").alias("hi_title"),
            FTSMonography.content.snippet("<mark>", "</mark>", max_tokens=64).alias("hi_content"),
        )
        .join(FTSMonography, on=(Monography.id == FTSMonography.rowid))
        .where(FTSMonography.match(term))
        .order_by(FTSMonography.bm25())
    )

    paginated_query = PaginatedQuery(
        query,
        paginate_by=RESULT_PAR_PAGES,
    )
    try:
        count = query.count()
    except peewee.OperationalError as e:
        # Malformed term ?
        return render_template(
            "search-full.html",
            error=str(e),
            term=term,
            title="Recherche",
        )

    return render_template(
        "results-full.html",
        results=paginated_query,
        count=count,
        term=term,
        title=f"{count} résultats de la recherche pour {term}",
    )


@main.route("/authors")
def authors() -> t.Text:
    """Displays the list of authors and their monographies"""
    authors = Author.select().order_by(Author.lastname)
    return render_template("authors.html", authors=authors, title="Les enquêteurs")


@main.route("/workers")
def workers() -> t.Text:
    """Displays the list of workers for each monography"""
    corpus = Monography.select().order_by(Monography.id)
    return render_template("workers.html", corpus=corpus, title="Index des ouvriers")


@main.route("/workers_old")
def workers_old() -> t.Text:
    """Displays the list of workers for each monography"""
    corpus = Monography.select().order_by(Monography.id)
    return render_template("workers_old.html", corpus=corpus, title="Index des ouvriers")


@main.route("/monographie/<string:slug>/workers")
def monography_workers(slug: str) -> t.Text:
    """Displays the list of workers for one monography"""
    try:
        monography = Monography.get(Monography.slug == slug)
    except Monography.DoesNotExist:
        abort(404)

    return render_template(
        "workers.html", corpus=[monography], title="Index des ouvriers pour " + monography.short_title
    )


@main.route("/about")
def about() -> t.Text:
    """Displays the About page"""
    return render_template("about.html", title="À propos")


@main.route("/contact")
def contact() -> t.Text:
    """Displays the Contact page"""
    return render_template("contact.html", title="Contact")


@main.route("/termsofservice")
def terms() -> t.Text:
    """Displaysthe T&C page."""
    return render_template("terms.html", title="Mentions légales")
