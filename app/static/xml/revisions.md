# Journal des révisions


| Id        | Titre                                              | Date correction | Commentaire |
| :---------|:---------------------------------------------------|:---------------:|:------------|
| ID-001a   | charpentier-paris-1                                | 04/08/22        |             |
| ID-002a   | manoeuvre-agriculteur-champagne-pouilleuse-2       | 04/08/22        |             |
| ID-003a   | paysans-communaute-lavedan-3                       | 08/08/22        |             |
| ID-004a   | paysan-labourd-4                                   | 02/09/22        |             |
| ID-005a   | metayer-banlieue-florence-5                        | 05/09/22        |             |
| ID-006a   | nourrisseur-vaches-banlieue-londres-6              | 07/09/22        |             |
| ID-007a   | tisseur-chales-fabrique-urbaine-collective-paris-7 | 09/09/22        |             |
| ID-008a   | manoeuvre-agriculteur-comte-nottingham-8           | 15/09/22        |             |
| ID-009a   | pecheur-cotier-maitre-barque-saint-sebastien-9     | 19/09/22        |             |
| ID-011a   | carrier-environs-paris-11                          | 22/09/22        |             |
| ID-012a   | menuisier-charpentier-tanger-12                    | 23/09/22        |             |
| ID-013a   | tailleur-habits-paris-13                           | 27/09/22        |             |
| ID-014a   | compositeur-typographe-bruxelles-14                | 29/09/22        |             |
| ID-015a   | decapteur-outils-acier-fabrique-herimoncourt-15    | 04/11/22        |             |
| ID-016a   | monteur-outils-acier-fabrique-herimoncourt-16      | 04/11/22        |             |
| ID-017a   | porteur-eau-paris-17                               | 17/06/24        |             |
| ID-018a   | paysans-communaute-polygamie-bousrah-18            | 18/06/24        |             |
| ID-019a   | debardeur-piocheur-craie-banlieue-paris-19         | 19/06/24        |             |
| ID-020a   | brodeuses-vosges-20                                | 24/06/24        |             |
| ID-021a   | paysan-savonnier-basse-provence-21                 | 09/07/24        |             |
| ID-022a   | mineur-placers-comte-mariposa-22                   | 18/07/24        |             |
| ID-023a   | manoeuvre-vigneron-aunis-23                        | 29/07/24        |             |
| ID-024a   | lingere-lille-24                                   | 31/07/24        |             |
| ID-025a   | parfumeur-tunis-25                                 | 28/08/24        |             |
| ID-026a   | institueur-primaire-commune-rurale-normandie-26    | 07/08/24        |             |
| ID-027a   | manoeuvre-famille-nombreuse-paris-27               | 30/08/24        |             |
| ID-028a   | fondeur-plomb-alpes-apuanes-28                     | 07/10/24        |             |
| ID-029a   | paysan-village-banlieue-morcelee-laonnais-29       | 14/10/24        |             |
| ID-030a   | paysans-communaute-nig-po-fou-30                   | 21/10/24        |             |
| ID-031a   | mineur-placers-comte-mariposa-22                   | 22/10/24        |             |

- ID-013a: ajouter le renvoi interne, ligne 366: "(D. 1ᵉ 8n)"
