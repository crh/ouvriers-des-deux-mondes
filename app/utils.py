import os
import typing as t


def get_filenames(path: str, extension: str) -> t.List[str]:
    """
    Making a list with files paths.
    :param path: path to a directory
    :type path: str
    :param extension: file extension
    :type extension: str
    :return: a dict
    :rtype: list
    """
    file_paths_ls = []
    for file in os.listdir(path):
        if file.endswith(extension):
            file_paths_ls.append(os.path.join(path, file))
    return file_paths_ls
