from flask.testing import FlaskClient


def test_index(client: FlaskClient) -> None:
    rv = client.get("/authors")
    assert rv.status_code == 200
