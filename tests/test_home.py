from flask.testing import FlaskClient


def test_home(client: FlaskClient) -> None:
    """Asserts home page is OK."""

    rv = client.get("/")
    assert rv.status_code == 200


def test_about(client: FlaskClient) -> None:
    """Asserts about page is OK."""

    rv = client.get("/about")
    assert rv.status_code == 200


def test_contact(client: FlaskClient) -> None:
    """Asserts contact page is OK."""

    rv = client.get("/contact")
    assert rv.status_code == 200


def test_termsofservice(client: FlaskClient) -> None:
    """Asserts T&C page is OK."""

    rv = client.get("/termsofservice")
    assert rv.status_code == 200


def test_privacy(client: FlaskClient) -> None:
    """Asserts privacy page is OK."""

    rv = client.get("/privacy")
    assert rv.status_code == 200
