import typing as t

import pytest
from flask.testing import FlaskClient

from app.modeles import Monography


def pytest_generate_tests(metafunc: t.Any) -> None:
    if "monography" in metafunc.fixturenames:
        metafunc.parametrize("monography", ["1", "2", "111 bis"], indirect=True)


@pytest.fixture
def monography(request: t.Any) -> Monography:
    return Monography.get(Monography.public_id == request.param)


def test_index(client: FlaskClient) -> None:
    rv = client.get("/monographies")
    assert rv.status_code == 200


def test_txt_mono(client: FlaskClient, monography: Monography) -> None:
    rv = client.get(f"/monographie/{monography.slug}")
    assert rv.status_code == 200


def test_download_xml(client: FlaskClient, monography: Monography) -> None:
    rv = client.get(f"/monographie/{monography.slug}/download/xml")
    assert rv.status_code == 200


def test_download_pdf(client: FlaskClient, monography: Monography) -> None:
    rv = client.get(f"/monographie/{monography.slug}/download/pdf")
    assert rv.status_code == 200
