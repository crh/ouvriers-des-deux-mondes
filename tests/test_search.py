import pytest
from flask.testing import FlaskClient


def test_search(client: FlaskClient) -> None:
    rv = client.get("/simple-search")
    assert rv.status_code == 200
    assert b"<h1>Recherche simple</h1>" in rv.data


terms = [
    ("proprietes", "4"),  # Animaux
    ("par_10", "18"),  # Vêtements
    ("travaux", "10"),  # chef de famille
    ("industries", "23"),  # famille
]

terms_404 = [
    ("foobar", "1"),  # type does not exist
    ("proprietes", "1337"),  # subtype does not exist
    ("proprietes", "23"),  # subtype not in type
]


@pytest.mark.parametrize("where,term", terms)
def test_search_results(client: FlaskClient, where: str, term: str) -> None:
    rv = client.get(f"/simple-search/{where}?term={term}")
    assert rv.status_code == 200
    # Search succeeds if there are results, displayed with <h3> headers
    # So we assume for testing that if there are any <h3...> tag, it is OK
    assert b"<h3" in rv.data


@pytest.mark.parametrize("where,term", terms_404)
def test_search_results_404(client: FlaskClient, where: str, term: str) -> None:
    rv = client.get(f"/simple-search/{where}?term={term}")
    assert rv.status_code == 404
