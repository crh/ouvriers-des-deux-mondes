import pytest
from flask.testing import FlaskClient

terms = [
    # Nominal cases with keywords/expressions
    "compagnons paris",
    '"prix de revient"',
    '"prix de revient" "salaire journalier"',
    '"prix de revient" paris',
    # FTS Operators
    "NEAR(prix maison)",
    "NEAR(prix maison, 15)",
    "agriculteur NOT paris",
    "agriculteur AND paris",
    "agriculteur OR éleveur",
    "(agriculteur OR éleveur) AND paris",
    # Quotes nominal
    '"prud\'hommes"',
    "prud'hommes",
    '"Conseil des prud\'hommes"',
    "Conseil des prud'hommes",
    # Unbalanced quotes
    "\"saint étienne d'horte",
    "saint étienne d'horte\"",
]


def test_searchfts(client: FlaskClient) -> None:
    rv = client.get("/search?term=")
    assert rv.status_code == 200
    assert b"<h1>Recherche</h1>" in rv.data


@pytest.mark.parametrize("term", terms)
def test_searchfts_results(client: FlaskClient, term: str) -> None:
    rv = client.get(f"/search?term={term}")
    assert rv.status_code == 200
    # Search succeeds if there are results, displayed with <h3> headers
    # So we assume for testing that if there are any <h3...> tag, it is OK
    assert b"<h3" in rv.data
