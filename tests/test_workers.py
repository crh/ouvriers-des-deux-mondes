from flask.testing import FlaskClient


def test_index(client: FlaskClient) -> None:
    rv = client.get("/workers")
    assert rv.status_code == 200
