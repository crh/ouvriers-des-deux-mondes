import os
import sys
import typing as t

sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)))
activate_this = os.path.abspath(os.path.dirname(__file__)) + "/.venv/bin/activate_this.py"

if os.path.exists(activate_this):
    with open(activate_this) as file_:
        exec(file_.read(), dict(__file__=activate_this))

    # On importe l'app uniquement APRES avoir activé le virtualenv
    from app import app

    app.config["FLASK_ENV"] = "production"

    def application(req_environ: dict, start_response: t.Callable) -> t.Any:
        return app(req_environ, start_response)
